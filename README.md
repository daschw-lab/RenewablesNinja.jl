# RenewablesNinja

This package is a thin Julia wrapper to access the [renewables.ninja API](https://www.renewables.ninja/documentation/api).

## Installation

If you have not already done so, add the [daschw-lab](https://codeberg.org/daschw-lab/daschw-lab)
registry to Julia.
Afterwards, you can use `Pkg` to add RenewablesNinja.

```julia
pkg> add RenewablesNinja
```

or

```julia
julia> import Pkg; Pkg.add("RenewablesNinja")
```

## Configuration

Anonymous access to the renewables.ninja API is limited to 5 requests per day.
You can create an account at https://renewables.ninja to increase the limit to 50 requests per hour.
If you have an account you can tell RenewablesNinja.jl by setting the environment variable `RENEWABLES_NINJA_TOKEN` to your account token.
You can find your token after login at https://renewables.ninja under `Profile > Token`.
You can set the environment variable at julia startup by adding
```
ENV["RENEWABLES_NINJA_TOKEN"] = "your-token"
```
to your julia startup file `~/.julia/config/startup.jl`.

---

By default all downloaded data is stored in `~/.julia/data/RenewablesNinja`.
If you want to save it in a different location, you can set the
`RENEWABLES_NINJA_DATA_PATH` environment variable, for example by adding the following line to
your Julia startup file `~/.julia/config/startup.jl`.

```julia
ENV["RENEWABLES_NINJA_DATA_PATH"] = "/path/to/store/your/data"
```

## Usage

```julia
using RenewablesNinja
```

RenewablesNinja exports two functions 
```julia
get_pv(; kwargs...) -> CSV.File
get_wind(; kwargs...) -> CSV.File
```
to get renewable production data specified by keyword arguments.
They return objects of type `CSV.File` (see [CSV.jl Documentation](https://csv.juliadata.org/stable/)) that can be converted to tables using e.g. [TypedTables.jl](https://typedtables.juliadata.org/stable/) or [DataFrames.jl](https://dataframes.juliadata.org/stable/).

Both functions support the following keyword arguments.
- `lat::Union{AbstractString, Real}`: Latitude. You can find the coordinates of places e.g. at https://www.latlong.net/.
- `lon::Union{AbstractString, Real}`: Longtitude. You can find the coordinates of places e.g. at https://www.latlong.net/.
- `date_from::Union{AbstractString, Dates.Date}`: Starting date for the requested data. For strings the format `"yyyy-mm-dd"` is expected.
- `date_to::Union{AbstractString, Dates.Date}`: Stopping date for the requested data. For strings the format `"yyyy-mm-dd"` is expected.
- `dataset::AbstractString`: `"merra2"` or `"sarah"`.
- `capacity::Union{AbstractString, Real}`: Capacity (kW).
- `raw::Bool`: If `true` raw weather data is included in the data too.
- `local_time::Bool`: If `true` a column with the local time is included in the data.
- `keep::Bool`: If `true` a local copy of the data is stored as CSV file. By default the file is saved in the current directy. To set a global directory for storing RenewablesNinja.jl files set the environment variable `RENEWABLES_NINJA_DATA_PATH`, for example by adding the line `ENV["RENEWABLES_NINJA_DATA_PATH"] = joinpath(homedir(), "RenewablesNinjaData")` to your julia startup file `~/.julia/config/startup.jl`.
- `update::Bool`: If `true` the data is downloaded again even if a local copy exists.

### PV

```julia
get_pv(;
    lat=48.20849,
    lon=16.37208,
    date_from=Date(2019, 1, 1),
    date_to=Date(2019, 12, 31),
    dataset="merra2",
    capacity=1,
    system_loss=0.1,
    tracking=0,
    tilt=35,
    azim=180,
    raw=false,
    local_time=false,
    keep=true,
    update=false,
)
```

#### Extra keyword arguments

- `system_loss::Union{AbstractString, Real}`: System loss (fraction) between 0 and 1.
- `tracking::Union{AbstractString, Integer}`: 0 (no tracking), 1 (1-axis azimuth) or 2 (2-axis tilt & azimuth).
- `tilt::Union{AbstractString, Real}`: Tilt (°). How far the panel is inclined from the horizontal, in degrees. A tilt of 0° is a panel facing directly upwards, 90° is a panel installed vertically, facing sideways.
- `azim::Union{AbstractString, Real}`: Azimuth (°). Compass direction the panel is facing (clockwise). An azimuth angle of 180 degrees means poleward facing, so for latitudes >=0 is interpreted as southwards facing, else northwards facing.
#### Example

```julia
using ReneablesNinja
using TypedTables
pv_table = Table(get_pv(lat=48, lon=16, date_from="2019-01-01", date_to="2019-12-31"))
```
```
Table with 2 columns and 8760 rows:
      time                 electricity
    ┌─────────────────────────────────
 1  │ 2019-01-01T00:00:00  0.0
 2  │ 2019-01-01T01:00:00  0.0
 3  │ 2019-01-01T02:00:00  0.0
 4  │ 2019-01-01T03:00:00  0.0
 5  │ 2019-01-01T04:00:00  0.0
 6  │ 2019-01-01T05:00:00  0.0
 7  │ 2019-01-01T06:00:00  0.0
 8  │ 2019-01-01T07:00:00  0.016
 9  │ 2019-01-01T08:00:00  0.046
 10 │ 2019-01-01T09:00:00  0.078
 11 │ 2019-01-01T10:00:00  0.11
 12 │ 2019-01-01T11:00:00  0.143
 13 │ 2019-01-01T12:00:00  0.135
 14 │ 2019-01-01T13:00:00  0.108
 15 │ 2019-01-01T14:00:00  0.035
 16 │ 2019-01-01T15:00:00  0.0
 ⋮  │          ⋮                ⋮
 ```

```julia
pv_weather_table = Table(
    get_pv(; lat=48, lon=16, date_from="2019-01-01", date_to="2019-12-31", raw=true)
)
```
```
Table with 5 columns and 8760 rows:
      time                 electricity  irradiance_direct  irradiance_diffuse  temperature
    ┌─────────────────────────────────────────────────────────────────────────────────────
 1  │ 2019-01-01T00:00:00  0.0          0.0                0.0                 -1.346
 2  │ 2019-01-01T01:00:00  0.0          0.0                0.0                 -1.215
 3  │ 2019-01-01T02:00:00  0.0          0.0                0.0                 -1.053
 4  │ 2019-01-01T03:00:00  0.0          0.0                0.0                 -0.865
 5  │ 2019-01-01T04:00:00  0.0          0.0                0.0                 -0.596
 6  │ 2019-01-01T05:00:00  0.0          0.0                0.0                 -0.153
 7  │ 2019-01-01T06:00:00  0.0          0.0                0.0                 0.286
 8  │ 2019-01-01T07:00:00  0.016        0.005              0.022               1.127
 9  │ 2019-01-01T08:00:00  0.046        0.008              0.054               2.07
 10 │ 2019-01-01T09:00:00  0.078        0.014              0.082               2.74
 11 │ 2019-01-01T10:00:00  0.11         0.023              0.106               3.325
 12 │ 2019-01-01T11:00:00  0.143        0.042              0.121               3.783
 13 │ 2019-01-01T12:00:00  0.135        0.047              0.108               3.885
 14 │ 2019-01-01T13:00:00  0.108        0.049              0.079               3.604
 ⋮  │          ⋮                ⋮               ⋮                  ⋮                ⋮
```

 ### Wind

 ```julia
get_wind(;
    lat=48.20849,
    lon=16.37208,
    date_from=Date(2019, 1, 1),
    date_to=Date(2019, 12, 31),
    dataset="merra2",
    capacity=1,
    height=80,
    turbine="Vestas+V90+2000",
    raw=false,
    local_time=false,
    keep=true,
    update=false,
)
```

#### Extra keyword arguments

- `height::Union{AbstractString, Real}`: Height of the wind turbine in meters.
- `turbine::AbstractString`: See `RenewablesNinja.TURBINE_MODELS` for available options.

#### Example

```julia
using RenewablesNinja
using DataFrames
wind_df = DataFrame(get_wind(lat=47, lon=22, date_from="2019-05-01", date_to="2019-05-31"))
```
```
744×2 DataFrame
 Row │ time                 electricity 
     │ DateTime…            Float64     
─────┼──────────────────────────────────
   1 │ 2019-05-01T00:00:00        0.351
   2 │ 2019-05-01T01:00:00        0.413
   3 │ 2019-05-01T02:00:00        0.453
   4 │ 2019-05-01T03:00:00        0.462
   5 │ 2019-05-01T04:00:00        0.437
   6 │ 2019-05-01T05:00:00        0.478
   7 │ 2019-05-01T06:00:00        0.556
  ⋮  │          ⋮                ⋮
 738 │ 2019-05-31T17:00:00        0.174
 739 │ 2019-05-31T18:00:00        0.274
 740 │ 2019-05-31T19:00:00        0.318
 741 │ 2019-05-31T20:00:00        0.354
 742 │ 2019-05-31T21:00:00        0.335
 743 │ 2019-05-31T22:00:00        0.326
 744 │ 2019-05-31T23:00:00        0.366
                        730 rows omitted
```

```julia
wind_weather_df = DataFrame(
    get_wind(; lat=47, lon=22, date_from="2019-05-01", date_to="2019-05-31", raw=true)
)
```
```
744×3 DataFrame
 Row │ time                 electricity  wind_speed 
     │ DateTime…            Float64      Float64    
─────┼──────────────────────────────────────────────
   1 │ 2019-05-01T00:00:00        0.401       7.454
   2 │ 2019-05-01T01:00:00        0.467       7.938
   3 │ 2019-05-01T02:00:00        0.51        8.241
   4 │ 2019-05-01T03:00:00        0.519       8.313
   5 │ 2019-05-01T04:00:00        0.496       8.141
   6 │ 2019-05-01T05:00:00        0.539       8.46
  ⋮  │          ⋮                ⋮           ⋮
 739 │ 2019-05-31T18:00:00        0.309       6.741
 740 │ 2019-05-31T19:00:00        0.355       7.105
 741 │ 2019-05-31T20:00:00        0.395       7.411
 742 │ 2019-05-31T21:00:00        0.377       7.272
 743 │ 2019-05-31T22:00:00        0.368       7.205
 744 │ 2019-05-31T23:00:00        0.413       7.533
                                    732 rows omitted
```

