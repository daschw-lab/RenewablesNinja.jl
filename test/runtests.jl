using DataFrames
using Dates
using RenewablesNinja
using Test
using TypedTables

date_from = Date(2019, 5, 6)
date_to = Date(2019, 5, 12)

pv_online = get_pv(; date_from=date_from, date_to=date_to, raw=true, update=true)
pv_local = get_pv(; date_from=date_from, date_to=date_to, raw=true)

wind_online = get_wind(;
    lat=45, lon=22, date_from=date_from, date_to=date_to, local_time=true, update=true
)
wind_local = get_wind(;
    lat=45, lon=22, date_from=date_from, date_to=date_to, local_time=true
)

@testset "RenewablesNinja.jl" begin
    @test Table(pv_online) == Table(pv_local)
    @test DataFrame(wind_online) == DataFrame(wind_local)
end
