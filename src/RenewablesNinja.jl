module RenewablesNinja

using CSV
using Dates
using HTTP

export get_pv, get_wind

function __init__()
    global TOKEN = get(ENV, "RENEWABLES_NINJA_TOKEN", nothing)
    global DATAPATH = get(ENV, "RENEWABLES_NINJA_DATA_PATH", joinpath(homedir(), ".julia", "data", "RenewablesNinja"))
    mkpath(DATAPATH)
end

const TOKEN_DOCS = """
Anonymous access to the renewables.ninja API is limited to 5 requests per day.
You can create an account at https://renewables.ninja to increase the limit to 50 requests per hour.
If you have an account you can tell RenewablesNinja.jl by setting the environment variable `RENEWABLES_NINJA_TOKEN` to your account token.
You can find your token after login at https://renewables.ninja under `Profile > Token`.
You can set the environment variable at julia startup by adding
```
ENV["RENEWABLES_NINJA_TOKEN"] = "your-token"
```
to your julia startup file `~/.julia/config/startup.jl`.
"""
const COMMON_PARAM_DOCS = """
- `lat::Union{AbstractString, Real}`: Latitude. You can find the coordinates of places e.g. at https://www.latlong.net/.
- `lon::Union{AbstractString, Real}`: Longtitude. You can find the coordinates of places e.g. at https://www.latlong.net/.
- `date_from::Union{AbstractString, Dates.Date}`: Starting date for the requested data. For strings the format `"yyyy-mm-dd"` is expected.
- `date_to::Union{AbstractString, Dates.Date}`: Stopping date for the requested data. For strings the format `"yyyy-mm-dd"` is expected.
- `dataset::AbstractString`: `"merra2"` or `"sarah"`.
- `capacity::Union{AbstractString, Real}`: Capacity (kW).
"""
const EXTRA_KEYWORD_DOCS = """
- `raw::Bool`: If `true` raw weather data is included in the data too.
- `local_time::Bool`: If `true` a column with the local time is included in the data.
- `keep::Bool`: If `true` a local copy of the data is stored as CSV file. By default the file is saved in the current directy. To set a global directory for storing RenewablesNinja.jl files set the environment variable `RENEWABLES_NINJA_DATA_PATH`, for example by adding the line `ENV["RENEWABLES_NINJA_DATA_PATH"] = joinpath(homedir(), "RenewablesNinjaData")` to your julia startup file `~/.julia/config/startup.jl`.
- `update::Bool`: If `true` the data is downloaded again even if a local copy exists. 
"""
const TURBINE_MODELS = [
    "Vestas+V90+2000",
    "Vestas+V47+660",
    "Vestas+V164+7000",
    "Siemens+SWT+2.3+93",
    "REpower+5M",
    "GE+1.5sle",
    "Enercon+E82+2000",
    "Enercon+E126+6500",
    "Acciona+AW77+1500",
    "Alstom+Eco+74",
    "Alstom+Eco+80",
    "Alstom+Eco+110",
    "Bonus+B23+150",
    "Bonus+B33+300",
    "Bonus+B37+450",
    "Bonus+B41+500",
    "Bonus+B44+600",
    "Bonus+B54+1000",
    "Bonus+B62+1300",
    "Bonus+B82+2300",
    "Dewind+D4+41+500",
    "Dewind+D6+1000",
    "Enercon+E40+500",
    "Enercon+E40+600",
    "Enercon+E44+900",
    "Enercon+E48+800",
    "Enercon+E53+800",
    "Enercon+E66+1500",
    "Enercon+E66+1800",
    "Enercon+E66+2000",
    "Enercon+E70+2000",
    "Enercon+E70+2300",
    "Enercon+E82+1800",
    "Enercon+E82+2300",
    "Enercon+E82+3000",
    "Enercon+E92+2300",
    "Enercon+E92+2350",
    "Enercon+E101+3000",
    "Enercon+E112+4500",
    "Enercon+E126+7000",
    "Enercon+E126+7500",
    "EWT+DirectWind+52+900",
    "Gamesa+G47+660",
    "Gamesa+G52+850",
    "Gamesa+G58+850",
    "Gamesa+G80+2000",
    "Gamesa+G87+2000",
    "Gamesa+G90+2000",
    "Gamesa+G128+4500",
    "GE+900S",
    "GE+1.5s",
    "GE+1.5se",
    "GE+1.5sl",
    "GE+1.5xle",
    "GE+1.6",
    "GE+1.7",
    "GE+2.5xl",
    "GE+2.75+103",
    "Goldwind+GW82+1500",
    "NEG+Micon+M1500+500",
    "NEG+Micon+M1500+750",
    "NEG+Micon+NM48+750",
    "NEG+Micon+NM52+900",
    "NEG+Micon+NM60+1000",
    "NEG+Micon+NM64c+1500",
    "NEG+Micon+NM80+2750",
    "Nordex+N27+150",
    "Nordex+N29+250",
    "Nordex+N43+600",
    "Nordex+N50+800",
    "Nordex+N60+1300",
    "Nordex+N80+2500",
    "Nordex+N90+2300",
    "Nordex+N90+2500",
    "Nordex+N100+2500",
    "Nordex+N131+3000",
    "Nordex+N131+3300",
    "Nordtank+NTK500",
    "Nordtank+NTK600",
    "PowerWind+56+900",
    "REpower+MD70+1500",
    "REPower+MD77+1500",
    "REpower+MM70+2000",
    "REpower+MM82+2000",
    "REpower+MM92+2000",
    "REpower+3+4M",
    "REpower+6M",
    "Siemens+SWT+1.3+62",
    "Siemens+SWT+2.3+82",
    "Siemens+SWT+2.3+101",
    "Siemens+SWT+3.0+101",
    "Siemens+SWT+3.6+107",
    "Siemens+SWT+3.6+120",
    "Siemens+SWT+4.0+130",
    "Suzlon+S88+2100",
    "Suzlon+S97+2100",
    "Tacke+TW600+43",
    "Vestas+V27+225",
    "Vestas+V29+225",
    "Vestas+V39+500",
    "Vestas+V42+600",
    "Vestas+V44+600",
    "Vestas+V52+850",
    "Vestas+V66+1650",
    "Vestas+V66+1750",
    "Vestas+V66+2000",
    "Vestas+V80+1800",
    "Vestas+V80+2000",
    "Vestas+V90+1800",
    "Vestas+V90+3000",
    "Vestas+V100+1800",
    "Vestas+V100+2000",
    "Vestas+V110+2000",
    "Vestas+V112+3000",
    "Vestas+V112+3300",
    "Wind+World+W3700",
    "Wind+World+W4200",
    "Windmaster+WM28+300",
    "Windmaster+WM43+750",
    "Windflow+500",
    "XANT+M21+100",
    "GE+3.2+103",
    "GE+3.2+130",
    "GE+3.4+130",
    "GE+3.8+130",
    "GE+5.3+158",
    "GE+5.5+158",
    "Gamesa+G128+5000",
    "Goldwind+GW109+2500",
    "Goldwind+GW121+2500",
    "Goldwind+GW140+3000",
    "Goldwind+GW140+3400",
    "Siemens+Gamesa+SG+4.5+145",
    "Siemens+SWT+3.6+130",
    "Siemens+SWT+4.1+142",
    "Siemens+SWT+4.3+130",
    "Vestas+V117+4000",
    "Vestas+V136+4000",
    "Vestas+V150+4000",
    "Vestas+V164+8000",
    "Vestas+V164+9500",
]

"""
    get_pv(;
        lat=48.20849,
        lon=16.37208,
        date_from=Date(2019, 1, 1),
        date_to=Date(2019, 12, 31),
        dataset="merra2",
        capacity=1,
        system_loss=0.1,
        tracking=0,
        tilt=35,
        azim=180,
        raw=false,
        local_time=false,
        keep=true,
        update=false,
    )

Get a `CSV.File` with pv production data specified by the keyword arguments.

$TOKEN_DOCS

## Keyword arguments
$COMMON_PARAM_DOCS
- `system_loss::Union{AbstractString, Real}`: System loss (fraction) between 0 and 1.
- `tracking::Union{AbstractString, Integer}`: 0 (no tracking), 1 (1-axis azimuth) or 2 (2-axis tilt & azimuth).
- `tilt::Union{AbstractString, Real}`: Tilt (°). How far the panel is inclined from the horizontal, in degrees. A tilt of 0° is a panel facing directly upwards, 90° is a panel installed vertically, facing sideways.
- `azim::Union{AbstractString, Real}`: Azimuth (°). Compass direction the panel is facing (clockwise). An azimuth angle of 180 degrees means poleward facing, so for latitudes >=0 is interpreted as southwards facing, else northwards facing.
$EXTRA_KEYWORD_DOCS
"""
function get_pv(;
    lat=48.20849,
    lon=16.37208,
    date_from=Date(2019, 1, 1),
    date_to=Date(2019, 12, 31),
    dataset="merra2",
    capacity=1,
    system_loss=0.1,
    tracking=0,
    tilt=35,
    azim=180,
    raw=false,
    local_time=false,
    keep=true,
    update=false,
)
    kw = (
        lat=lat,
        lon=lon,
        date_from=date_from,
        date_to=date_to,
        dataset=dataset,
        capacity=capacity,
        system_loss=system_loss,
        tracking=tracking,
        tilt=tilt,
        azim=azim,
        raw=raw,
        local_time=local_time,
        format="csv",
    )
    return get_data("pv", kw, keep, update)
end

"""
    get_wind(;
        lat=48.20849,
        lon=16.37208,
        date_from=Date(2019, 1, 1),
        date_to=Date(2019, 12, 31),
        dataset="merra2",
        capacity=1,
        height=80,
        turbine="Vestas+V90+2000",
        raw=false,
        local_time=false,
        keep=true,
        update=false,
    )

Get a `CSV.File` with wind production data specified by the keyword arguments.

$TOKEN_DOCS

## Keyword arguments
$COMMON_PARAM_DOCS
- `height::Union{AbstractString, Real}`: Height of the wind turbine in meters.
- `turbine::AbstractString`: See `RenewablesNinja.TURBINE_MODELS` for available options.
$EXTRA_KEYWORD_DOCS
"""
function get_wind(;
    lat=48.20849,
    lon=16.37208,
    date_from=Date(2019, 1, 1),
    date_to=Date(2019, 12, 31),
    dataset="merra2",
    capacity=1,
    height=80,
    turbine="Vestas+V90+2000",
    raw=false,
    local_time=false,
    keep=true,
    update=false,
)
    kw = (
        lat=lat,
        lon=lon,
        date_from=date_from,
        date_to=date_to,
        dataset=dataset,
        capacity=capacity,
        height=height,
        turbine=turbine,
        raw=raw,
        local_time=local_time,
        format="csv",
    )
    return get_data("wind", kw, keep, update)
end

function get_data(type, kw, keep, update)
    fn = joinpath(DATAPATH, string(type, "_", get_file_params(kw)))
    if isfile(fn) && !update
        return CSV.File(fn)
    end
    url = string("https://www.renewables.ninja/api/data/", type, "?", get_params(kw))
    file = get_file(url)
    if keep
        mkpath(DATAPATH)
        CSV.write(fn, file)
    end
    return file
end

function get_file(url)
    req = request_data(url, TOKEN)
    return CSV.File(req.body; header=4, dateformat="yyyy-mm-dd HH:MM")
end

request_data(url, token) = HTTP.get(url, Dict("Authorization" => "Token $token"))
request_data(url, ::Nothing) = HTTP.get(url)

get_params(kw) = join((string(k, "=", v) for (k, v) in pairs(kw)), "&")
get_file_params(kw) = join((string(k, "=", v) for (k, v) in pairs(kw)), "_")

end
